/*

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2017 Dyachihin Andrey

 */
package org.bitbucket.kris_1.code_example.scheduler.impl;

import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class DateGenerator
{
    final private long offsetFromNowMsec;
    final private int rangeMsec;

    public Date get() {
        long now = System.currentTimeMillis();
        long val = now + offsetFromNowMsec + ThreadLocalRandom.current().nextInt(rangeMsec);
        return new Date(val);
    }

    public DateGenerator(int offsetFromNowMsec, int rangeMsec) {
        if (rangeMsec < 0)
            throw new IllegalArgumentException("rangeMsec < 0");
        this.offsetFromNowMsec = offsetFromNowMsec;
        this.rangeMsec = rangeMsec;
    }

    @Override
    public String toString() {
        return "DateGenerator [offsetFromNowMsec=" + offsetFromNowMsec + ", rangeMsec=" + rangeMsec + "]";
    }
}
