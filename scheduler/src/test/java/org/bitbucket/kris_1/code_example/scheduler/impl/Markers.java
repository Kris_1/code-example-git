/*

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2017 Dyachihin Andrey

 */
package org.bitbucket.kris_1.code_example.scheduler.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Markers
{
    private static final Logger log = LoggerFactory.getLogger(Markers.class);

    final private static long NOT_DONE = Long.MIN_VALUE;

    final private ConcurrentHashMap<UUID, long[]> markers = new ConcurrentHashMap<>();
    final private ConcurrentSkipListSet<Long> occuringDates = new ConcurrentSkipListSet<>();
    final private List<String> errors = Collections.synchronizedList(new ArrayList<String>(100));

    public UUID get() {
        return UUID.randomUUID();
    }

    public void register(UUID uuid, Date occuringOn) {
        if (uuid == null)
            throw new NullPointerException("uuid");
        if (occuringOn == null)
            throw new NullPointerException("occuringOn");
        long[] dts = new long[2];
        dts[0] = occuringOn.getTime();
        dts[1] = NOT_DONE;
        occuringDates.add(dts[0]);
        if (markers.put(uuid, dts) != null)
            errors.add("Duplicate register " + uuid);
    }

    public void done(UUID uuid, long occuredOn) {
        long[] dts = markers.get(uuid);
        if (dts == null) {
            errors.add("Not found " + uuid);
            return;
        }
        synchronized (dts) {
            if (dts[1] != NOT_DONE)
                errors.add("Is done " + uuid);
            dts[1] = occuredOn - dts[0];
        }
    }

    public long maxOccuringOn() {
        return occuringDates.last();
    }

    public void throwErrors() {
        if (errors.isEmpty())
            return;
        RuntimeException ex = new RuntimeException();
        for (String err : errors)
            ex.addSuppressed(new RuntimeException(err));
        throw ex;
    }

    public Collection<UUID> getNotDone() {
        ArrayList<UUID> result = new ArrayList<>();
        for (Map.Entry<UUID, long[]> entry : markers.entrySet()) {
            if (entry.getValue()[1] == NOT_DONE) {
                log.error("Not processed {} {}", entry.getKey(), entry.getValue()[0]);
                result.add(entry.getKey());
            }
        }
        return result;
    }

    public Collection<Long> getOutOfRange(int min, int max) {
        ArrayList<Long> result = new ArrayList<>();
        for (Map.Entry<UUID, long[]> entry : markers.entrySet()) {
            long pTime = entry.getValue()[1];
            if (pTime == NOT_DONE)
                continue;
            if (pTime < min || pTime > max) {
                log.error("Out of range {} {} {}", entry.getKey(), entry.getValue()[0], entry.getValue()[1]);
                result.add(pTime);
            }
        }
        return result;
    }
}
