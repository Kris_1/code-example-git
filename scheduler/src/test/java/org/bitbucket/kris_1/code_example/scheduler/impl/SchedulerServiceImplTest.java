/*

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2017 Dyachihin Andrey

 */
package org.bitbucket.kris_1.code_example.scheduler.impl;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Phaser;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.bitbucket.kris_1.code_example.scheduler.api.Callable;
import org.bitbucket.kris_1.code_example.scheduler.api.SchedulerService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchedulerServiceImplTest
{
    private static final Logger log = LoggerFactory.getLogger(SchedulerServiceImplTest.class);

    private ThreadPoolExecutor createExec() {
        return new ThreadPoolExecutor(
            3,
            3,
            0,
            TimeUnit.MILLISECONDS,
            new ArrayBlockingQueue<Runnable>(1000),
            new ThreadPoolExecutor.CallerRunsPolicy());
    }

    private void checkGenerator(
        final SchedulerService sched,
            final Markers markers,
            final DateGenerator occuredGenerator,
            final int planerThread,
            final int marksNumber,
            Phaser rootPh) {
        final Phaser ph = new Phaser(rootPh);
        ph.register();
        for (int i = 0; i < planerThread; i++) {
            ph.register();
            new Thread()
            {
                @Override
                public void run() {
                    ph.arriveAndAwaitAdvance();
                    for (int j = 0; j < marksNumber / planerThread; j++) {
                        final UUID marker = markers.get();
                        final Date occuringOn = occuredGenerator.get();
                        markers.register(marker, occuringOn);
                        sched.put(occuringOn, new Callable()
                        {
                            @Override
                            public void call() {
                                markers.done(marker, System.currentTimeMillis());
                            }
                        });
                    }
                    ph.arriveAndDeregister();
                };
            }.start();
        }
        ph.arriveAndDeregister();
    }

    private void loadGenerator(
        final SchedulerService sched,
            final DateGenerator occuredGenerator,
            final int planerThread,
            final int maxAwaitMsec,
            Phaser rootPh) {
        final Phaser ph = new Phaser(rootPh);
        ph.register();
        for (int i = 0; i < planerThread; i++) {
            ph.register();
            Thread th = new Thread()
            {
                final private Random rnd = new Random();

                @Override
                public void run() {
                    ph.arriveAndAwaitAdvance();
                    int phase = ph.arrive();
                    while (true) {
                        try {
                            ph.awaitAdvanceInterruptibly(phase, rnd.nextInt(maxAwaitMsec), TimeUnit.MILLISECONDS);
                            ph.arriveAndDeregister();
                            return;
                        } catch (TimeoutException ex) {
                        } catch (InterruptedException ex) {
                            throw new RuntimeException(ex);
                        }
                        final Date occuringOn = occuredGenerator.get();
                        sched.put(occuringOn, new Callable()
                        {
                            @Override
                            public void call() {
                            }
                        });
                    }
                };
            };
            th.start();
        }
        ph.arriveAndDeregister();
    }

    @Test
    public void testWideRange() throws Exception {
        log.warn("starting {}", System.currentTimeMillis());
        final int marksNumber = 1000;
        ThreadPoolExecutor exec = createExec();
        SchedulerServiceImpl sched = new SchedulerServiceImpl(exec);
        Markers markers = new Markers();
        Phaser ph = new Phaser();
        ph.register();
        checkGenerator(sched, markers, new DateGenerator(100, 100000), 4, marksNumber, ph);
        ph.arriveAndAwaitAdvance();
        loadGenerator(sched, new DateGenerator(10, 10000), 4, 100, ph);
        ph.arriveAndAwaitAdvance();
        Thread.sleep(markers.maxOccuringOn() - System.currentTimeMillis() + 1000);
        ph.arriveAndAwaitAdvance();
        markers.throwErrors();
        assertEquals(Collections.emptyList(), markers.getNotDone());
        if (markers.getOutOfRange(0, 10).size() > marksNumber / 100)
            fail("Too many out of range");
    }

    @Test
    public void testNarrowRange() throws Exception {
        log.warn("starting {}", System.currentTimeMillis());
        ThreadPoolExecutor exec = createExec();
        SchedulerServiceImpl sched = new SchedulerServiceImpl(exec);
        Markers markers = new Markers();
        Phaser ph = new Phaser();
        ph.register();
        checkGenerator(sched, markers, new DateGenerator(100, 10000), 4, 1000, ph);
        ph.arriveAndAwaitAdvance();
        loadGenerator(sched, new DateGenerator(10, 10000), 4, 100, ph);
        ph.arriveAndAwaitAdvance();
        Thread.sleep(markers.maxOccuringOn() - System.currentTimeMillis() + 1000);
        ph.arriveAndAwaitAdvance();
        markers.throwErrors();
        assertEquals(Collections.emptyList(), markers.getNotDone());
        assertEquals(Collections.emptyList(), markers.getOutOfRange(0, 10));
    }

    @Test
    public void testSuperNarrowRange() throws Exception {
        log.warn("starting {}", System.currentTimeMillis());
        ThreadPoolExecutor exec = createExec();
        SchedulerServiceImpl sched = new SchedulerServiceImpl(exec);
        Markers markers = new Markers();
        Phaser ph = new Phaser();
        ph.register();
        checkGenerator(sched, markers, new DateGenerator(100, 1000), 4, 1000, ph);
        ph.arriveAndAwaitAdvance();
        loadGenerator(sched, new DateGenerator(10, 10000), 4, 100, ph);
        ph.arriveAndAwaitAdvance();
        Thread.sleep(markers.maxOccuringOn() - System.currentTimeMillis() + 1000);
        ph.arriveAndAwaitAdvance();
        log.warn("finished {}", System.currentTimeMillis());
        markers.throwErrors();
        assertEquals(Collections.emptyList(), markers.getNotDone());
        assertEquals(Collections.emptyList(), markers.getOutOfRange(0, 10));
    }

    @Test
    public void testBorder() throws Exception {
        log.warn("starting {}", System.currentTimeMillis());
        ThreadPoolExecutor exec = createExec();
        SchedulerServiceImpl sched = new SchedulerServiceImpl(exec);
        Markers markers = new Markers();
        Phaser ph = new Phaser();
        ph.register();
        checkGenerator(sched, markers, new DateGenerator(-5, 5), 4, 100000, ph);
        ph.arriveAndAwaitAdvance();
        loadGenerator(sched, new DateGenerator(-5, 5), 4, 100, ph);
        ph.arriveAndAwaitAdvance();
        Thread.sleep(markers.maxOccuringOn() - System.currentTimeMillis() + 1000);
        ph.arriveAndAwaitAdvance();
        markers.throwErrors();
        assertEquals(Collections.emptyList(), markers.getNotDone());
        assertEquals(Collections.emptyList(), markers.getOutOfRange(0, 200));
    }

    @Test
    public void testOverload() throws Exception {
        log.warn("starting {}", System.currentTimeMillis());
        ThreadPoolExecutor exec = createExec();
        SchedulerServiceImpl sched = new SchedulerServiceImpl(exec);
        Markers markers = new Markers();
        Phaser ph = new Phaser();
        ph.register();
        checkGenerator(sched, markers, new DateGenerator(200, 1), 10, 1000000, ph);
        ph.arriveAndAwaitAdvance();
        loadGenerator(sched, new DateGenerator(200, 5), 10, 10, ph);
        ph.arriveAndAwaitAdvance();
        Thread.sleep(markers.maxOccuringOn() - System.currentTimeMillis() + 1000);
        ph.arriveAndAwaitAdvance();
        markers.throwErrors();
        assertEquals(Collections.emptyList(), markers.getNotDone());
    }
}
