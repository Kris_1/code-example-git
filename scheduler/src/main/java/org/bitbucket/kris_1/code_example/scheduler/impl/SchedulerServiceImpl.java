/*

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2017 Dyachihin Andrey

 */
package org.bitbucket.kris_1.code_example.scheduler.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.bitbucket.kris_1.code_example.scheduler.api.Callable;
import org.bitbucket.kris_1.code_example.scheduler.api.SchedulerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Особенности имплементации:
 * <ul>
 * <li>
 * Данный код оптимизирован для работы с событиями, которые отстоят от текущеного времени относительно далеко (200
 * милисекунд и более). При добавлении событий, которые срабатывают через менее чем через секунду, возможно отклонение
 * от запланированного времени на {@link SchedulerServiceImpl#oldCollectorMsec} милисекунд.</li>
 * <li>
 * Данный код оптимизирован для работы с событиями, которые равномерно распределены на промежутке (коллизии по времени
 * возможны, но редки).</li>
 * </ul>
 *
 * @author Andrey
 *
 */
public class SchedulerServiceImpl implements SchedulerService
{
    private static final Logger log = LoggerFactory.getLogger(SchedulerServiceImpl.class);

    final private Executor exec;
    final private ConcurrentSkipListMap<Long, ConcurrentLinkedQueue<Callable>> events = new ConcurrentSkipListMap<>();
    final private ReentrantLock lock = new ReentrantLock();
    final private Condition firstChanged = lock.newCondition();

    final private int oldTimeoutMsec;
    final private int oldCollectorMsec;

    private void execute(final Callable callable) {
        exec.execute(new Runnable()
        {
            @Override
            public void run() {
                callable.call();
            }
        });
    }

    @Override
    public void put(Date occuringOn, Callable callable) {
        if (occuringOn == null)
            throw new NullPointerException("occuringOn");
        if (callable == null)
            throw new NullPointerException("callable");
        long dt = occuringOn.getTime();
        if (dt < System.currentTimeMillis()) {
            execute(callable);
            return;
        }
        boolean isNew = false;
        ConcurrentLinkedQueue<Callable> calls = new ConcurrentLinkedQueue<>();
        {
            ConcurrentLinkedQueue<Callable> oldCalls = events.putIfAbsent(dt, calls);
            if (oldCalls != null)
                calls = oldCalls;
            else
                isNew = true;
        }
        calls.add(callable);
        if (isNew) {
            Map.Entry<Long, ?> entry = events.firstEntry();
            if (entry != null && entry.getKey() == dt) {
                lock.lock();
                try {
                    entry = events.firstEntry();
                    if (entry != null && entry.getKey() == dt)
                        firstChanged.signal();
                } finally {
                    lock.unlock();
                }
            }
        }
    }

    final private class SchRunner extends Thread
    {
        TreeMap<Long, List<ConcurrentLinkedQueue<Callable>>> oldCalls = new TreeMap<>();

        private void process(ConcurrentLinkedQueue<Callable> calls) {
            for (Callable call = calls.poll(); call != null; call = calls.poll())
                execute(call);
        }

        private void processEvents(long now) {
            for (Map.Entry<Long, ConcurrentLinkedQueue<Callable>> entry = events.firstEntry(); entry != null
                && now >= entry.getKey(); entry = events.firstEntry()) {
                Long dt = entry.getKey();
                ConcurrentLinkedQueue<Callable> calls = events.remove(dt);
                process(calls);
                List<ConcurrentLinkedQueue<Callable>> lt = oldCalls.get(dt);
                if (lt == null) {
                    lt = new ArrayList<>(10);
                    oldCalls.put(dt, lt);
                }
                lt.add(calls);
            }
        }

        private void processOlds(long now) {
            for (Map.Entry<Long, List<ConcurrentLinkedQueue<Callable>>> entry = oldCalls.firstEntry(); entry != null
                && now >= entry.getKey(); entry = oldCalls.firstEntry()) {
                List<ConcurrentLinkedQueue<Callable>> lt = oldCalls.remove(entry.getKey());
                for (ConcurrentLinkedQueue<Callable> calls : lt)
                    process(calls);
            }
        }

        private void awaitNextEvent() throws InterruptedException {
            lock.lock();
            try {
                long wait;
                do {
                    Map.Entry<Long, ?> entry = events.firstEntry();
                    Long dt = entry == null ? Long.MAX_VALUE : entry.getKey();
                    wait = dt - System.currentTimeMillis();
                    if (wait <= 0)
                        break;
                    if (!oldCalls.isEmpty() && wait > oldCollectorMsec)
                        wait = oldCollectorMsec;
                    else
                        wait = dt - System.currentTimeMillis();
                } while (firstChanged.await(wait, TimeUnit.MILLISECONDS));
            } finally {
                lock.unlock();
            }
        }

        @Override
        public void run() {
            try {
                while (!isInterrupted()) {
                    try {
                        long now = System.currentTimeMillis();
                        processEvents(now);
                        processOlds(now - oldTimeoutMsec);
                        awaitNextEvent();
                    } catch (InterruptedException ex) {
                        throw ex;
                    } catch (Exception ex) {
                        log.error("Exception", ex);
                    }
                }
            } catch (InterruptedException ex) {
                log.debug("Schedule thread is interupted");
            }
        }
    }

    public SchedulerServiceImpl(Executor exec, int oldTimeoutMsec, int oldCollectorMsec) {
        if (exec == null)
            throw new NullPointerException("exec");
        this.exec = exec;
        this.oldTimeoutMsec = oldTimeoutMsec;
        this.oldCollectorMsec = oldCollectorMsec;
        Thread sch = new SchRunner();
        sch.setDaemon(true);
        sch.start();
    }

    public SchedulerServiceImpl(Executor exec) {
        this(exec, 1000, 500);
    }
}
